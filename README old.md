# Explanation

### JSON 
— The Zoe JSON contains the required payloads to make the cluster work. I don’t have Arduino code to implement it, but you simply take the data from the json, put it into structure of the type

struct zoe_can_packet {
	uint8_t  payload[8];
	uin32_t can_id;
	uint32_t frequency; (in milliseconds)
}

Then you just make a NON BLOCKING code which sends every packet at the specific frequency. Then you can play a bit with the payloads to change the stuff the cluster displays (try random fuzzing — quite fun)

### REGARDING THE CLUSTER AND HARDWARE
##### CAN BUS
* CAN-H is CAN high, CAN-L is CAN low, just connect the Cables correctly to the shields and cluster (take a picture of the cluster btw, in case the cables come out)
* Black is ground and red is Red is power, but you need a +12V to turn in on (and I forgot to give you one I think). Is safer if you use a standard one, so you can drop by to my office today/tomorrow to get one, or you can go next week in office 2.21 and ask the guy there, I can leave him the hardware!
* My OBD logger code. The hardware is composed by a Arduino mega + CAN shield, a gps shield, an SD card reader (I forgot to give you an SD card, in case ask me), and the STN OBD interpreter (to talk the OBD protocol).
* you can power in via the USB+5V or via the VIN / DB9 adapter at +12V

### Useful documentation:
- https://en.wikipedia.org/wiki/CAN_bus
- https://en.wikipedia.org/wiki/On-board_diagnostics
- http://illmatics.com/carhacking.html
- http://wiki.seeedstudio.com/CAN-BUS_Shield_V1.2/
- https://www.sparkfun.com/products/9555

### Exercises
Just take a look at the code, and play a bit with the CAN shields and Zoe cluster. Then we can discuss the details!
However, what I can imagine is implementable:

1. Logging. The device should log CAN, GPS, and possibly other sensor data, if available (so would be nice to have an abstraction where a new sensor would have just to implement the <To_string()> and <log> methods). This essentially consists in cleaning my code.

2. Possibility of uploading the logged data to the cloud.

3. Update over the air capabilities.

4. SSH capabilities, ideally over ssh I can some specific commands like ’start logging, stop logging etc.’ and also maybe inject packets to the car.
For example:
-> Fuzzing (I.e., a command over ssh / serial, and the device perform fuzzing)
-> Reply attacks with modification (i.e., I set a filter that, whenever I see a packet in the CAN, the device send a different one)
-> General purpose packet sending (e.g., send the packet X every Y milliseconds etc.)
