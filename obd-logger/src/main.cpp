#include "interfaces.h"
#include "streams.h"
#include <SPI.h>
#include <SoftwareSerial.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>

// PINS
#define INTERRUPT_PIN 2
#define SDCARD_PIN 4
#define MCP_CAN_PIN 9
#define GREEN_LED_PIN 8

//Enable sleep when CAN is silent
#define enable_sleep 1
volatile uint8_t flagRecv = 0;

//Globals
ArduinoOutStream cout(Serial);          //Global cout stream
SDInterface sdcard(SDCARD_PIN);         //SD card -- Pin 4
GPSInterface gps(&Serial1, &sdcard);    //GPS - Arduino Mega HardwareSerial(1)  RX = 19, TX = 18
CANInterface can(MCP_CAN_PIN, &sdcard); //CAN Shield -- Pin 9
OBDInterface obd(&Serial2, &sdcard);    // OBD - Arduino Mega HardwareSerial(2) RX=17, TX=16

//Logger
char __sdlogstr[logstr_size];
char __sdfmtstr[fmtstr_size];

//Variables and const
const uint64_t can_time_limit = 60000UL; //60 seconds of no CAN activity
const uint64_t gps_log_time = 5000UL;    //5 seconds

uint64_t gps_timer = 0;
uint64_t can_timer = 0;
unsigned long log_time0 = 0;

//Led last state change
uint64_t led_last_state_change = 0;
uint8_t led_state = LOW;

void update_led() {
    //if can undetected for more than 5 sec, led blinks fast
    if (millis() - can_timer >= 5000UL) {
        if (millis() - led_last_state_change >= 1000) {
            digitalWrite(GREEN_LED_PIN, (led_state == LOW ? HIGH : LOW));
            led_state = (led_state == LOW ? HIGH : LOW);
            led_last_state_change = millis();
        }
        return;
    }
    //led ON if everything fine
    digitalWrite(GREEN_LED_PIN, HIGH);
}

//Blink led for sleep/awake
void blink_led() {
    int i = 50;
    do {
        if (millis() - led_last_state_change >= 50) {
            digitalWrite(GREEN_LED_PIN, (led_state == LOW ? HIGH : LOW));
            led_state = (led_state == LOW ? HIGH : LOW);
            led_last_state_change = millis();
            i--;
        }
    } while (i >= 0);
    led_last_state_change = millis();
    digitalWrite(GREEN_LED_PIN, LOW);
    led_state = LOW;
}

//Interrupt handler
void MCP2515_ISR() {
    flagRecv = 1;
    detachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN));
}

void endTrip() {
    flagRecv = 0;
    sdcard.close_log();
    LOG_DEBUG(&sdcard, "%s", "Trip ended");
    cout.flush();
}

void startTrip() {
    flagRecv = 1;
    sdcard.init_log();
    log_time0 = gps_timer = can_timer = millis();
    LOG_DEBUG(&sdcard, "%s", "Trip started");
}

void enterSleepMode() {
    blink_led();
    endTrip();
    delay(1000);
#if enable_sleep
    pinMode(INTERRUPT_PIN, INPUT);
    attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN), MCP2515_ISR, LOW);
    can.mcp.sleep();
    gps.gps.standby();
    obd.sleep();
    delay(2000);
    cli();
    if (!flagRecv) {
        set_sleep_mode(SLEEP_MODE_PWR_DOWN);
        sleep_enable();
        sei();
        sleep_cpu();
        sleep_disable();
    }
    sei();
    can.mcp.wake();
    gps.gps.wakeup();
    obd.wakeup();
#else
    delay(5000);
#endif
    startTrip();
    blink_led();
}

void setup_interrupt() {
#if enable_sleep
    can.mcp.setSleepWakeup(1);
    can.mcp.mcpPinMode(MCP_RX0BF, MCP_PIN_INT); //Set-up only when going to sleep!
    can.mcp.mcpPinMode(MCP_RX1BF, MCP_PIN_INT);
#endif
}

void setup() {
    Serial.begin(115200);
    sdcard.begin();
    gps.begin();
    can.begin();
    obd.begin();
    setup_interrupt();
    //Setup LEDs
    pinMode(GREEN_LED_PIN, OUTPUT);
    startTrip();
}

void loop() {
    //Receive up to 10 frames per cycle
    for (int i = 0; i < 10; i++) {
        if (!can.recvFrame()) {
            break;
        }
        flagRecv = 1;
        //Do not consider OBD frames for timer
        if (!can.last_frame_obd)
            can_timer = millis();
#if __DEBUG_MODE && __CAN_VERBOSE
        can.printRecvFrame();
#endif
    }

    //Trigger next OBD state.
    obd.update();
#if __DEBUG_MODE
    if (Serial.available()) {
        unsigned char x = Serial.read();
        if (x == '\n')
            Serial2.flush();
        else
            Serial2.write(x);
    }
#endif

    //GPS Updates and log
    gps.update();
    if (millis() - gps_timer >= gps_log_time) {
#if __DEBUG_MODE && __GPS_VERBOSE
        gps.printInfo();
#endif
        gps.logLocation();
        sdcard.log_bitrate();
        gps_timer = millis();
    }

    //Update LEDS
    update_led();

    //Not received CAN frames for a long while; disable stuff and go to sleep
    if (millis() - can_timer >= can_time_limit) {
        enterSleepMode();
    }
}
