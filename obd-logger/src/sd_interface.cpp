#include "interfaces.h"
#include "streams.h"

void SDInterface::begin() {
    if (!this->sd.begin(this->chip_select, SD_SCK_MHZ(50))) {
        sd.initErrorHalt();
    }
}

void SDInterface::log_bitrate() {
    double dt = ((double)(millis() - this->init_time)) / 1000.0; //Delta time in seconds
    double br = (((double)this->byte_written) / 1000.0) / dt;    //KByte per second
    double ld = br / 100.0;                                      //Max is currently on the order of 100KBps/sec
    char sbr[16], sload[16];
    dtostrf(br, 4, 2, sbr);
    dtostrf(ld, 6, 4, sload);
    LOG_DEBUG(this, "SD: KByte/sec=%s, load=%s", sbr, sload);
    //Reset timers
    this->init_time = millis();
    this->byte_written = 0;
}

void SDInterface::log_str(SdFile *file, const char *str, bool force_flush) {
    if (!file->isOpen()) {
        return;
    }
    size_t wr = file->write(str);
    this->byte_written += wr;

    if (millis() - this->last_log_time >= 500) {
        force_flush = true;
    }

    if (force_flush) {
        if (!file->sync() || file->getWriteError()) {
            cout << "Error in syncing file." << endl;
        }
        this->last_log_time = millis();
    }
}

void SDInterface::log_debug(const char *str) {
    this->log_str(&(this->debug_file), str, true);
}

void SDInterface::log_data(const char *str) {
    this->log_str(&(this->log_file), str, false);
}

void SDInterface::close_log() {
    if (!this->log_file.close() || !this->debug_file.close()) {
        cout << "Error closing log files." << endl;
    }
}

bool SDInterface::init_log() {
    char folder_name[32];
    unsigned int x = 0;
    do {
        memset(folder_name, 0x00, 32);
        sprintf(folder_name, "log_%u", x);
        x++;
    } while (this->sd.exists(folder_name) && (x < 0xFFFF));
    return this->init_log(folder_name);
}

bool SDInterface::init_log(char folder_name[32]) {
    char log_buffer[64], debug_buffer[64];
    //Does folder exists?
    if (this->sd.exists(folder_name)) {
        cout << "Error: " << folder_name << " already exists!" << endl;
        return false;
    }
    //Create it
    if (!this->sd.mkdir(folder_name)) {
        cout << "Error in creating directory " << folder_name << endl;
        return false;
    }
    //File to log everything
    strcpy(log_buffer, folder_name);
    strcat(log_buffer, "/log.dat");
    if (!this->log_file.open(log_buffer, O_WRONLY | O_CREAT)) {
        cout << "Error in opening file " << log_buffer << endl;
        return false;
    }
    //Only to log errors!
    strcpy(debug_buffer, folder_name);
    strcat(debug_buffer, "/debug.log");
    if (!this->debug_file.open(debug_buffer, O_WRONLY | O_CREAT)) {
        cout << "Error in opening file " << debug_buffer << endl;
        return false;
    }
    //Sync to be sure they exists
    this->debug_file.sync();
    this->log_file.sync();
    //Start bit rate
    this->init_time = millis();
    this->byte_written = 0;
    return true;
}

void SDInterface::print_info() {
    cid_t cid;
    if (!this->sd.card()->readCID(&cid)) {
        this->sd.errorHalt(F("readCID failed"));
    }
    cout << F("\nManufacturer ID: ") << hex << int(cid.mid) << dec << endl;
    cout << F("OEM ID: ") << cid.oid[0] << cid.oid[1] << endl;
    cout << F("Product: ");
    for (uint8_t i = 0; i < 5; i++)
        cout << cid.pnm[i];
    cout << F("\nVersion: ") << int(cid.prv_n) << '.' << int(cid.prv_m) << endl;
    cout << F("Serial number: ") << hex << cid.psn << dec << endl;
    cout << F("Manufacturing date: ") << int(cid.mdt_month) << '/';
    cout << (2000 + cid.mdt_year_low + 10 * cid.mdt_year_high) << endl;
    cout << endl;
    cout << F("Type is FAT") << int(this->sd.vol()->fatType()) << endl;
    cout << F("Card size: ") << this->sd.card()->cardSize() * 512E-9 << F(" GB (GB = 1E9 bytes)") << endl;
}