#include <mcp_can.h>
#include <SdFat.h>
#include <Adafruit_GPS.h>

class SDInterface {
public:
  const uint8_t chip_select;
  uint64_t byte_written = 0;
  uint64_t init_time = 0;
  uint64_t last_log_time = 0;

  SdFat sd;
  SdFile log_file, debug_file;

  SDInterface(uint8_t chip_select) : chip_select(chip_select){};
  void begin();
  void print_info();

  bool init_log();
  void close_log();

  void log_debug(const char *str);
  void log_data(const char *str);
  void log_bitrate();

private:
  void log_str(SdFile *file, const char *str, bool force_flush);
  bool init_log(char folder_name[32]);
};

class GPSInterface {
public:
  Adafruit_GPS gps;
  SDInterface *sdcard;
  GPSInterface(HardwareSerial *ser, SDInterface *sd) : gps(ser), sdcard(sd){};

  void begin();
  void update();
  void logLocation();
  void printInfo();
};

class OBDInterface
{
private:
  typedef enum OBD_STATES
  {
    set_echo_off = 0x00,
    set_uart_speed,
    set_header,
    set_header_response,
    set_search_proto,
    obd_first_query,
    obd_query,
    silence
  } obd_state_t;

public:
  HardwareSerial *ser;
  SDInterface *sdcard;

  char sendBuf[128];
  char recvBuf[128];
  bool isRecv = false;
  int recvBufIDX = 0;
  int state = 0;
  bool ready = true;
  bool waitingAck = false;
  int currPid  = -1;
  int currPrio = 0;
  uint64_t lastQuery = 0;

  OBDInterface(HardwareSerial *ser, SDInterface *sd) : ser(ser), sdcard(sd){};

  void begin();
  void update();

  void sleep();
  void wakeup();

  void changeState(obd_state_t next, const char *cmd);
  bool sendCommand(const char *cmd, uint64_t send_timeout, bool force);
  bool recvNonBlock();
  bool recvBlock(uint64_t recv_timeout);

  void readySendBuffer(const char *cmd);
  bool sendBlock(uint64_t send_timeout);
  bool sendNonBlock(bool force);

  bool firstOBDQuery();
  void queryOBD();
  void nextPrioPid();
};

class CANInterface
{
public:
  MCP_CAN mcp;
  SDInterface *sdcard;

  uint32_t recv_id;
  uint8_t recv_len;
  uint8_t recv_payload[8];
  bool    last_frame_obd = false;

  CANInterface(uint8_t chip_select, SDInterface *sd) : mcp(chip_select), sdcard(sd){};

  bool begin();
  bool recvFrame();
  void printRecvFrame();
};
