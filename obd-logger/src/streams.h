#ifndef __MY_STREAMS_H__
#define __MY_STREAMS_H__

#include <SdFat.h>

#define logstr_size 512
#define fmtstr_size 128

extern ArduinoOutStream cout;
extern char __sdlogstr[logstr_size];
extern char __sdfmtstr[fmtstr_size];
extern unsigned long log_time0;

#define __DEBUG_MODE 1
#define __DEBUG_VERBOSE 0
#define __GPS_VERBOSE 0
#define __OBD_VERBOSE 0
#define __CAN_VERBOSE 0

//forward declaration of sdcard
class SDInterface;

inline void LOG_DEBUG(SDInterface *sd, const char *format, ...) {
    //Generate format string
    snprintf(__sdfmtstr, fmtstr_size, "%lu.%lu,%s\n", millis() - log_time0, (micros() % 1000), format);
    //Argument list
    va_list valist;
    va_start(valist, format);
    //Generate log string
    vsnprintf(__sdlogstr, logstr_size, __sdfmtstr, valist);
    va_end(valist);
    //Output and log
    sd->log_debug(__sdlogstr);
#if __DEBUG_MODE
    cout << "DEBUG: " << __sdlogstr;
#endif
}

inline void LOG_DATA(SDInterface *sd, const char *format, ...) {
    //Generate format string
    snprintf(__sdfmtstr, fmtstr_size, "%lu.%lu,%s\n", millis() - log_time0, (micros() % 1000), format);
    //Argument list
    va_list valist;
    va_start(valist, format);
    //Generate log string
    vsnprintf(__sdlogstr, logstr_size, __sdfmtstr, valist);
    va_end(valist);
    sd->log_data(__sdlogstr);
#if __DEBUG_VERBOSE
    cout << "DATA:  " << __sdlogstr;
#endif
}

#endif