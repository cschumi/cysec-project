#include "interfaces.h"
#include "streams.h"

bool CANInterface::begin() {
    for (int x = 0; x < 10; x++) {
        if (CAN_OK == this->mcp.begin(CAN_500KBPS))
            return true;
        delay(100);
    }
    LOG_DEBUG(this->sdcard, "%s", "CAN: Failed to start CAN/MCP.");
    return false;
}

//Logging as msec.usec,CAN:id,len,b0...b8
bool CANInterface::recvFrame() {
    if (this->mcp.checkReceive() == CAN_MSGAVAIL) {
        //Put to zero
        this->recv_id = 0;
        this->recv_len = 0;
        memset(this->recv_payload, 0x00, 8);
        //Read frame
        this->mcp.readMsgBufID(&(this->recv_id), &(this->recv_len), this->recv_payload);
        //Set OBD flag
        this->last_frame_obd = false;
        if (this->mcp.isExtendedFrame()) {
            if (this->recv_id >= 0x18DAF100)
                last_frame_obd = true;
        } else {
            if (this->recv_id >= 0x7D0)
                last_frame_obd = true;
        }
        LOG_DATA(this->sdcard,
                 "F,%lu,%d,%02X,%02X,%02X,%02X,%02X,%02X,%02X,%02X",
                 this->recv_id, (int)this->recv_len,
                 this->recv_payload[0], this->recv_payload[1], this->recv_payload[2], this->recv_payload[3],
                 this->recv_payload[4], this->recv_payload[5], this->recv_payload[6], this->recv_payload[7]);
        return true;
    }
    return false;
}

void CANInterface::printRecvFrame() {
    Serial.print("CAN: (ID=");
    Serial.print(this->recv_id, HEX);
    Serial.print(")[ ");
    for (uint8_t i = 0; i < this->recv_len; i++) {
        Serial.print(this->recv_payload[i], HEX);
        Serial.print(" ");
    }
    Serial.println("]");
}