#include "interfaces.h"
#include "streams.h"

void GPSInterface::begin() {
    // 9600 NMEA is the default baud rate for Adafruit MTK GPS's- some use 4800
    this->gps.begin(9600);
    // uncomment this line to turn on RMC (recommended minimum) and GGA (fix data) including altitude
    this->gps.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCGGA);
    // For the parsing code to work nicely and have time to sort thru the data, and print it out we don't suggest using anything higher than 1 Hz
    this->gps.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ);
    // Request updates on antenna status, comment out to keep quiet
    this->gps.sendCommand(PGCMD_ANTENNA);
    delay(1000);
}

void GPSInterface::update() {
    this->gps.read();
    if (this->gps.newNMEAreceived()) {
        if (!this->gps.parse(this->gps.lastNMEA()))
            return;
    }
}

void GPSInterface::logLocation() {
    //Prepare string with all floating point values
    char buffer[16], temp[128];
    memset(temp, 0x0, 128);
    //strcat(strcat(temp, dtostrf((double)this->gps.latitude, 8, 6, buffer)), ",");
    strcat(strcat(temp, dtostrf((double)this->gps.latitudeDegrees, 8, 6, buffer)), ",");
    //strcat(strcat(temp, dtostrf((double)this->gps.longitude, 8, 6, buffer)), ",");
    strcat(strcat(temp, dtostrf((double)this->gps.longitudeDegrees, 8, 6, buffer)), ",");
    //strcat(strcat(temp, dtostrf((double)this->gps.speed, 4, 2, buffer)), ",");
    strcat(strcat(temp, dtostrf((double)this->gps.altitude, 6, 3, buffer)), ",");
    //strcat(strcat(temp, dtostrf((double)this->gps.angle, 4, 2, buffer)), ",");
    LOG_DATA(this->sdcard, "G,%d,%d,%d,%d,%d,%d,%d,%s", this->gps.year, this->gps.month, this->gps.day, this->gps.hour, this->gps.minute, this->gps.seconds, (int)this->gps.fix, temp);
}

void GPSInterface::printInfo() {
    cout << "=== GPS ===" << endl;
    cout << "Time: " << (int)this->gps.hour << ":" << (int)this->gps.minute << ":" << (int)this->gps.seconds
         << ", Date: " << (int)this->gps.day << "/" << (int)this->gps.month << "/20" << (int)this->gps.year
         << ", Fix: " << this->gps.fix << ". Quality: " << (int)this->gps.fixquality << endl;
    if (this->gps.fix) {
        cout << "Location: " << this->gps.latitude << this->gps.lat << ", " << this->gps.longitude << this->gps.lat;
        cout << ", Location (maps compatible): " << this->gps.latitudeDegrees << ", " << this->gps.longitudeDegrees << endl;
        cout << "Speed (knots): " << this->gps.speed << ", Altitude: " << this->gps.altitude << ", Angle: " << this->gps.angle << ", Satellites: " << (int)this->gps.satellites << endl;
    }
}
