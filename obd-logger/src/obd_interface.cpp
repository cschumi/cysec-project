#include "interfaces.h"
#include "obd_pid.h"
#include "streams.h"

/*
TODO
    Transform this to a STN1110 or ELM327 interface to be able to A) Send/Recv OBD and B) Log frames
    via interface. CAN frames should be also managed here in case of MCP2515 chip. 
    Generally, frames/message should be encodable for UPD, Serial, SPI, or SD card.

TODO 
    Do the same for GPS module (make module/chip abstraction), SD card (simplify SDFat)

Idenally, everything should be possible only via UDP or over Serial. Support WiFi and GSM Logging etc.
*/

#define ELM_TIMEOUT 1500
#define ELM_EOF '\r'  //End of message
#define ELM_READY '>' //Ready to receive

#define ELM_RESET "ATZ"           //Reset the ELM
#define ELM_ECHO_OFF "ATE 0"      //Command echo OFF
#define ELM_HEADER_ON "ATH 1"     //CAN headers ON
#define ELM_SEARCH_PROTO "ATSP 0" //Automatically search for protocol

#define STN_BAUD115200 "STBR 115200"  //Swtich UART Baud speed
#define STN_SLEEPMODE "STSLEEP 5"     //Enter sleep mode after 5sec
#define STN_SETWAKEUP "STUART off,on" //Enable UART WakeUp --> Need to send a pulse

void OBDInterface::begin() {
    this->ser->begin(9600);
    delay(ELM_TIMEOUT);
    //Send Initial reset
    this->readySendBuffer(ELM_RESET);
    this->sendNonBlock(true);
    LOG_DEBUG(this->sdcard, "%s", "OBD: ATZ ok");
    //Start state machine
    this->changeState(obd_state_t::set_uart_speed, STN_BAUD115200);
}

void OBDInterface::sleep() {
    this->readySendBuffer(STN_SLEEPMODE);
    this->sendBlock(ELM_TIMEOUT);
}

void OBDInterface::wakeup() {
    this->readySendBuffer("WAKEUP!");
    this->sendNonBlock(true);
    //Reset PIDs query
    this->currPid = -1;
    this->currPrio = 0;
    this->state = obd_state_t::set_uart_speed;
}

void OBDInterface::changeState(obd_state_t next, const char *cmd) {
    this->state = next;
    this->readySendBuffer(cmd);
}

bool OBDInterface::firstOBDQuery() {
    if (this->waitingAck) {
        if (!this->isRecv) {
            return false;
        }
        //Check If I've received until now a reply containing either a 'NO DATA' or an OBD frame in hex.
        PidQuery *p = &pid_list[this->currPid];
        if (strncmp(this->recvBuf, "NO DATA", strlen("NO DATA")) == 0) {
            p->is_present = false;
            this->waitingAck = false;
        } else {
            //Discard SEARCHING... and OK. Else assume is an OBD Frame and that is present
            if (strncmp(this->recvBuf, "SEARCHING...", strlen("SEARCHING...")) == 0)
                return false;
            if (strncmp(this->recvBuf, "OK", strlen("OK")) == 0)
                return false;
            p->is_present = true;
        }
        //Check if I have more Pids to send... If not I can move to next stage
        if (this->currPid == obd_num_pids - 1) {
            return true;
        }
    }
    //Wait 500ms to make sure all replys are done.
    if (millis() - this->lastQuery <= 250) {
        return false;
    }

    //Send next request, return false as we stop only when receiving the last reply
    PidQuery *p = &pid_list[this->currPid + 1];
    this->readySendBuffer(p->toQuery());
    if (this->sendNonBlock(false)) {
        this->currPid++;
        this->waitingAck = true;
        this->lastQuery = millis();
    }
    return false;
}

void OBDInterface::queryOBD() {
    //If I've a PID in memory, try to send it. After, move to next PID
    bool next_pid = (this->currPid == -1 ? true : false);
    if (this->currPid >= 0) {
        if (millis() - this->lastQuery >= 50) {
            next_pid = this->sendNonBlock(false);
        }
    }

    if (!next_pid)
        return;
    this->lastQuery = millis();

    //Find the next PID in the category
    PidQuery *p = NULL;
    do {
        //If no more PIDs, update cateogry and switch
        if (this->currPid == obd_num_pids - 1) {
            this->currPrio++;
            if (this->currPrio == obd_num_prio)
                this->currPrio = 0;
            this->currPid = 0;
        }
        this->currPid++;
        p = &pid_list[this->currPid];
    } while (p->prio != prio_list[this->currPrio] || !p->is_present);

    //Prepare next send buffer
    if (p) {
        this->readySendBuffer(p->toQuery());
    }
}

#define CASE(A, B, C, D)                      \
    case A: {                                 \
        if (this->sendNonBlock(false)) {      \
            this->changeState(B, C);          \
            LOG_DEBUG(this->sdcard, "%s", D); \
        }                                     \
        break;                                \
    }

//State machine that write stuff if possible to write, or wait for answer if waiting.
void OBDInterface::update() {
    //This implicitly log when complete.
    this->recvNonBlock();
    switch (this->state) {
        CASE(obd_state_t::set_echo_off, obd_state_t::set_header, ELM_HEADER_ON, "OBD: Echo OFF.");
        CASE(obd_state_t::set_header, obd_state_t::set_search_proto, ELM_SEARCH_PROTO, "OBD: Headers OK.");
        CASE(obd_state_t::set_search_proto, obd_state_t::obd_first_query, "", "OBD: Serach protocol OK.");

    case obd_state_t::set_uart_speed:
        if (this->sendBlock(ELM_TIMEOUT)) {
            this->changeState(obd_state_t::set_echo_off, ELM_ECHO_OFF);
            LOG_DEBUG(this->sdcard, "%s", "OBD: UART speed set.");
        }
        break;

    case obd_state_t::obd_first_query:
        if (this->firstOBDQuery()) {
            this->currPid = -1;
            this->currPrio = 0;
            this->changeState(obd_state_t::obd_query, "");
            LOG_DEBUG(this->sdcard, "%s", "OBD: Initial PIDs serach completed");
            //Count Pids that are aviable
            bool allFalse = true;
            for (size_t i = 0; i < obd_num_pids; i++) {
                if (pid_list[i].is_present) {
                    allFalse = false;
                    break;
                }
            }
            if (allFalse) {
                this->changeState(obd_state_t::silence, "");
                LOG_DEBUG(this->sdcard, "%s", "OBD: No answer to PID. Sleeping.");
            }
        }
        break;

    case obd_state_t::obd_query:
        this->queryOBD();
        break;

    default:
        break;
    }
}

void OBDInterface::readySendBuffer(const char *cmd) {
    sprintf(this->sendBuf, "%s%c", cmd, ELM_EOF);
}

bool OBDInterface::sendBlock(uint64_t send_timeout) {
    uint64_t timeout = millis();
    do {
        if (this->sendNonBlock(false))
            return true;
    } while (millis() - timeout < send_timeout);
    return false;
}

bool OBDInterface::sendNonBlock(bool force) {
    //Check if the <ELM_READY> char is in the serial.
    this->recvNonBlock();
    if (this->ready || force) {
        this->ser->write(this->sendBuf);
        this->ready = false;
        LOG_DATA(this->sdcard, "O,>,%s", this->sendBuf);
#if __OBD_VERBOSE
        cout << "OBD:>" << this->sendBuf << endl;
#endif
        return true;
    }
    return false;
}

bool OBDInterface::recvBlock(uint64_t recv_timeout) {
    uint64_t timeout = millis();
    do {
        if (this->recvNonBlock())
            return true;
    } while (millis() - timeout < recv_timeout);
    return false;
}

bool OBDInterface::recvNonBlock() {
    while (this->ser->available()) {
        char x = this->ser->read();
        if (x == ELM_READY) {
            this->ready = true;
            continue;
        }
        if (x == ELM_EOF) {
            if (this->recvBufIDX == 0)
                return false;
            this->recvBuf[this->recvBufIDX] = '\0';
            this->recvBufIDX = 0;
            this->isRecv = true;
            LOG_DATA(this->sdcard, "O,<,%s", this->recvBuf);
#if __OBD_VERBOSE
            cout << "OBD:<" << this->recvBuf << endl;
#endif
            return true;
        } else {
            this->recvBuf[this->recvBufIDX++] = x;
            this->isRecv = false;
        }
    }
    return false;
}
