#ifndef __OBD_PIDS_H__
#define __OBD_PIDS_H__

//Sequence of questions --> once --> high --> medium --> high --> medium --> high --> low --> high --> medium
typedef enum __pid_priority {
    once = 0,
    high,
    medium,
    low
} pid_priority_t;

pid_priority_t prio_list[] = {
    pid_priority_t::high,
    pid_priority_t::medium,
    pid_priority_t::high,
    pid_priority_t::medium,
    pid_priority_t::high,
    pid_priority_t::low,
};

class PidQuery
{
public:
    char           pidbuf[8];
    const uint8_t  mode;
    const uint8_t  func;
    const pid_priority_t prio;
    bool is_present = true;

    PidQuery(uint8_t mode, uint8_t func, pid_priority_t prio) : mode(mode), func(func), prio(prio){};
    PidQuery(uint8_t mode, uint8_t func): PidQuery(mode, func, pid_priority_t::once){};

    const char * toQuery(){
        sprintf(pidbuf, "%02X%02X", this->mode, this->func);
        return this->pidbuf;
    }
};

PidQuery pid_list[] = {
    PidQuery(0x01, 0x03, pid_priority_t::low),    //fuel system status
    PidQuery(0x01, 0x04, pid_priority_t::medium), //engine load
    PidQuery(0x01, 0x05, pid_priority_t::low),    //engine coolant temperature
    PidQuery(0x01, 0x0A, pid_priority_t::low),    //fuel pressure
    PidQuery(0x01, 0x0C, pid_priority_t::high),   //engine RPM
    PidQuery(0x01, 0x0D, pid_priority_t::high),   //Vehicle speed
    PidQuery(0x01, 0x0E, pid_priority_t::low),    //timing advance
    PidQuery(0x01, 0x0F, pid_priority_t::low),    //intake air temperature

    PidQuery(0x01, 0x11, pid_priority_t::high), //throttle position
    PidQuery(0x01, 0x14, pid_priority_t::low),  //oxygen sensor 1
    PidQuery(0x01, 0x15, pid_priority_t::low),  //oxygen sensor 2
    PidQuery(0x01, 0x1F, pid_priority_t::low),  //run-time since engine start

    PidQuery(0x01, 0x22, pid_priority_t::low), //fuel rail pressure
    PidQuery(0x01, 0x23, pid_priority_t::low), //fuel rail gauge pressure
    PidQuery(0x01, 0x24, pid_priority_t::low), //oxygen sensor 1
    PidQuery(0x01, 0x25, pid_priority_t::low), //oxygen sensor 2
    PidQuery(0x01, 0x2F, pid_priority_t::low), //fuel tank level input

    PidQuery(0x01, 0x32, pid_priority_t::low), //evaporator system pressure
    PidQuery(0x01, 0x33, pid_priority_t::low), //absolute barometric pressure
    PidQuery(0x01, 0x3C, pid_priority_t::low), //catalyst temperature 1

    PidQuery(0x01, 0x41, pid_priority_t::low),    //monitor status of drive cycle
    PidQuery(0x01, 0x42, pid_priority_t::medium), //control module voltage
    PidQuery(0x01, 0x43, pid_priority_t::medium), //absolute load voltage
    PidQuery(0x01, 0x45, pid_priority_t::high),   //relative throttle position
    PidQuery(0x01, 0x46, pid_priority_t::low),    //ambient air temperature
    PidQuery(0x01, 0x47, pid_priority_t::high),   //absolute throttle position A
    PidQuery(0x01, 0x49, pid_priority_t::high),   //accelerator pedal position D
    PidQuery(0x01, 0x4C, pid_priority_t::medium), //commanded throttle actuator

    PidQuery(0x01, 0x5C, pid_priority_t::low),    //engine oil temperature
    PidQuery(0x01, 0x5E, pid_priority_t::medium), //engine fuel-rate
    PidQuery(0x01, 0x61, pid_priority_t::high),   //driver's demand torque
    PidQuery(0x01, 0x62, pid_priority_t::high),   //actual engine percent torque
    PidQuery(0x01, 0x64, pid_priority_t::high),   //engine perfect torque data
    PidQuery(0x01, 0x67, pid_priority_t::low),    //engine coolant temp
    PidQuery(0x01, 0xA6, pid_priority_t::low),    //odometer

    PidQuery(0x09, 0x02, pid_priority_t::once), //vin
    PidQuery(0x09, 0x04, pid_priority_t::once), //calibration id
    PidQuery(0x09, 0x06, pid_priority_t::once), //calibration verification number (vnc)
    PidQuery(0x09, 0x08, pid_priority_t::once), //in-use performance tracking for spark vehicle
    PidQuery(0x09, 0x0A, pid_priority_t::once), //ecu name
    PidQuery(0x09, 0x0B, pid_priority_t::once)  //in-use performance tracking for compression ignition vehicle
};

const int obd_num_pids = (int)(sizeof(pid_list)  / sizeof(PidQuery));
const int obd_num_prio = (int)(sizeof(prio_list) / sizeof(pid_priority_t));

#endif