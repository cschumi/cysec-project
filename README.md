# CAN Pyboard Logger
### Functions
* Logging
* Over-the-air updates
* Uploading currently logged data
* Invoke commands (planned)

### Logging
This program logs all CAN data packages and in addition writes the current time and location to the log.

### Over-the-air (OTA)
When booting up (turn of the ignition key) the device automatically checks if a new version has been published via the "version.txt". If available it automatically downloads the update and applies it.

### Uploading current log
When receiving a phone call via the GSM module, the data will be uploaded to a server and afterwards be deleted from the SD card.

### Invoke commands
It is planned to be able to provide certain commands online and on call to invoke those commands.